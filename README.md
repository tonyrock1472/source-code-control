# README #

This README would normally document whatever steps are necessary to get your application up and running.

### The repository is to keep track of my prototype, CLEAN YOUR ROOM ###

-Create Blueprint level
	-Floating items for the player to collect
	-add exploding objects to distract the player from completing the goal
-Create HUD display to show progress 
	-as player collects items, reflect on the HUD display (the goal)
-Create player movements and actions
	-easy to manuver controls (binding WASD)
	
-Tested destroy/spawn actor in blueprint
	- binding the F key to spawn and when pressed again destroys actor
-Add feature to the prototype
	-the destroy/spawn actor could be used towards the exloding factors of the prototype
	
-Add button trigger actor
	-Binding the E key to open door
	-when player enters the the box (area where the function would work) player could press E to open door

### How do I get set up? ###

-Start off in Unreal engine
	-after making a change in the blueprint Compile and save to test.

